<!-- TOC -->autoauto- [VirtualBox installation](#virtualbox-installation)auto- [Debian VM setup](#debian-vm-stup)auto- [Ansible Inventory](#ansible-inventory)auto- [Sample Ansible playbook](#sample-ansible-playbook)autoauto<!-- /TOC -->

# VirtualBox installation

* Started by following this link:
  * <https://computingforgeeks.com/install-virtualbox-on-kali-linux-linux-mint/>
* Then ran into a version conflict, between the virtualbox and virtualbox-dkms.
* Fixed by switching from VirtualBox 6.1 to 5.2. Easiest way out, as Mint comes with this version of dkms.

# Debian VM stup

* Download a Debian image from *osboxes*:
  * <https://www.osboxes.org/debian/#debian-10-info>
* Note the following about this image:
  * Qwerty (US) keyboard layout.
  * Username & password are listed in the tab *Info*.
* The downloaded Debian image is an *ova* file. Choose *Important appliance* in VirtualBox and select given *ova* file.
* Adjust some settings of our new VM:
  * Enable Shared clipboard. In the Section General/Advanced.
  * Memory & processors.
  * Network. Set to "Bridged Adapter", so that the VM receives it own IP in the network.
* Update the file */etc/apt/sources.list*:
  * The default version is full of crap.
  * See example file in: <https://wiki.debian.org/SourcesList.>
  * Note that *sudo* does not work here. Switch to super user using the command *su*.
* Install some basic software:
  * *apt-get update*
  * *apt-get install vim openssh-sherver*
* Update ssh server config:
  * */etc/ssh/sshd_config*
* Start the ssh server:
  * *systemctl start sshd*
* Check your ip: *ip a*

# Ansible Inventory

* Install *sshpass* if you want to authenticate to target machines using username & password
  * *sudo apt-get install sshpass*
  * Only needed if not using public key based authentication
* Setup public key based authentication:
  * *cat ~/.ssh/id_rsa.pub | ssh username@target "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"*
* Create inventory file *inventory.txt*
  * Default inventory file: */etc/ansible/hosts*
* Example inventory to start from:
  * *target1 ansible_host=192.168.0.252 ansible_user=osboxes ansible_ssh_private_key_file=~/.ssh/id_rsa*
  * *target2 ansible_host=192.168.0.169 ansible_user=osboxes ansible_ssh_pass=osboxes.org*
* Test if all is working fine:
  * ansible target1 -m ping -i inventory.txt
  * ansible target2 -m ping -i inventory.txt
* To find out the ip-addresses of the target VM's:
  * Either login & check via the command *ip a*
  * Or use *nmap*
* Using nmap:
  * My local machine is on 192.168.0.something, with netmask 255.255.255.0
  * Therefore, I want to scan the range 192.168.0.0 until 192.168.0.255
  * A simple ping scan will do: *sudo nmap -sn 192.168.0.0/24*
  * There will be two devices whose MAC address mention *Oracle VirtualBox NIC*

# Sample Ansible playbook

* Create a very simple playbook, just to test if we are up and running.
* Run the playbook for given inventory: *ansible-playbook playbook.yml -i inventory.txt*
